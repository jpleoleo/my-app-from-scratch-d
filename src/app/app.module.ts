import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { Page1Component } from './components/page1/page1.component';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { Page2Component } from './components/page2/page2.component';
import { AuthenticationGuard } from './services/authentication.guard';
import { NavigationModule } from './components/navigation/navigation.module';
import { AppCustomPreloader } from './services/custom-preload.sevice';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'page-1', component: Page1Component, canActivate: [AuthenticationGuard] },
  { path: 'page-2', component: Page2Component, canActivate: [AuthenticationGuard] },
  { path: 'lazy', loadChildren: './components/lazy/lazy.module#LazyModule', canActivate: [AuthenticationGuard] },
  { path: 'lazy-1', loadChildren: './components/lazy1/lazy1.module#Lazy1Module', canActivate: [AuthenticationGuard], data: { preload: true }  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    Page1Component,
    Page2Component
  ],
  imports: [
    BrowserModule,
    //RouterModule.forRoot(routes),//Enable this to load all Lazy Modules in demand
    //RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),//Enable preload of all modules
    RouterModule.forRoot(routes, { preloadingStrategy: AppCustomPreloader }),//Enable preload for specific modules
    FormsModule,
    NavigationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
