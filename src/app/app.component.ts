import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalDataService } from './services/global-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app-from-scratch-a';

  constructor(private router:Router, private globalDataService:GlobalDataService){

  }

  ngOnInit(){
    if(this.globalDataService.data.validUser){
      this.router.navigateByUrl('page-1');
    }else{
      this.router.navigateByUrl('login');
    }
  }
}
