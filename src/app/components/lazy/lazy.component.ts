import { Component, OnInit } from '@angular/core';
import { GlobalDataService } from '../../services/global-data.service';

@Component({
  selector: 'app-lazy',
  templateUrl: './lazy.component.html',
  styleUrls: ['./lazy.component.css']
})
export class LazyComponent implements OnInit {

  constructor(private globalDataService:GlobalDataService) { }

  ngOnInit() {
    this.globalDataService.data.currentPage = 'lazy';
    this.globalDataService.data.pages.lazy.visited = true;
    this.globalDataService.data.pages.lazy.visitCount++;
  }

}
