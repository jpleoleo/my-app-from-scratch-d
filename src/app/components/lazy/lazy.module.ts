import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LazyComponent } from './lazy.component';
import { RouterModule, Routes } from '@angular/router';
import { NavigationModule } from '../navigation/navigation.module';

const routes: Routes = [
  { path: '', redirectTo: 'load-me' },
  { path: 'load-me', component: LazyComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NavigationModule
  ],
  declarations: [LazyComponent]
})
export class LazyModule { }
