import { Component, OnInit } from '@angular/core';
import { GlobalDataService } from '../../services/global-data.service';

@Component({
  selector: 'app-lazy1',
  templateUrl: './lazy1.component.html',
  styleUrls: ['./lazy1.component.css']
})
export class Lazy1Component implements OnInit {

  constructor(private globalDataService:GlobalDataService) { }

  ngOnInit() {
    this.globalDataService.data.currentPage = 'lazy-1';
    this.globalDataService.data.pages.lazy.visited = true;
    this.globalDataService.data.pages.lazy.visitCount++;
  }

}
