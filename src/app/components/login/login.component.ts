import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalDataService } from '../../services/global-data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  credential = {
    email:'',
    password:''
  }
  constructor(private router:Router, private globalDataService:GlobalDataService) { }

  ngOnInit() {
    if(this.globalDataService.data.validUser){
      if(this.globalDataService.data.currentPage){
        this.router.navigateByUrl(this.globalDataService.data.currentPage);
      }else{
        this.router.navigateByUrl('page-1')
      }
    }else{
      this.globalDataService.data = JSON.parse(JSON.stringify(this.globalDataService.initialData));
    }
  }

  signIn(){
    if(this.credential.email==='abc@gmail.com' && this.credential.password === '1234'){
      this.globalDataService.data.validUser = true;
      this.router.navigateByUrl('page-1');
    }else{
      console.warn('username anad password is not match');
      this.credential.password = '';
    }
  }

}
